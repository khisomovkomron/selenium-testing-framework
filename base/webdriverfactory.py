from selenium import webdriver

class WebDriverFactory():
    def __init__(self, browser):
        self.browser = browser

    def getWebDriverInstance(self):

        baseURL = "https://learn.letskodeit.com/"

        if self.browser = "iexplorer":
            #set IE driver
            driver = webdriver.Ie()
        elif self.browser = "firefox":
            #set Firefox driver
            driver = webdriver.Firefox()
        elif self.browser = "chrome":
            #set Chrome driver
            drover = webdriver.Chrome()
        else:
            driver = webdriver.Firefox()

        driver.implicitly_wait(3)
        driver.maximize_window()
        driver.get(baseURL)
        return driver